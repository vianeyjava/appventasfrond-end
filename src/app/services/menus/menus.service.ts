import { Injectable } from '@angular/core';
import { AppSettings } from '../../app.settings';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ModelMenu } from '../../models/menus/ModelMenu';
import { TOKEN_NAME, HOST } from '../../components/shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class MenusService {

  // private apiUrl = AppSettings.API_ENDPOINT + 'Menus';
  private url: string = `${HOST}Menus`;
  menuCambio = new Subject<ModelMenu[]>();

  constructor(public http: HttpClient) { }

  getMenus(): Observable<ModelMenu[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelMenu[]>(`${this.url}/getAll`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });

  }

  listarPorUsuario(nombre: string) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelMenu[]>(`${this.url}/listarMenuUsuario/${nombre}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
}
