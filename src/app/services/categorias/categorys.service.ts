import { race } from 'rxjs/internal/operators';
import { AppSettings } from '../../app.settings';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { of, Subject } from 'rxjs';
import { ModelCategorias } from '../../models/inventory/categorias/ModelCategorys';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { ICategorias } from '../../interfaces/ICategorias';
import { HOST, TOKEN_NAME } from '../../components/shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class CategorysService {

  // private apiUrl = AppSettings.API_ENDPOINT + 'Categorys';
  private apiUrl: string = `${HOST}Categorys`;
  categoryCambio = new Subject<ModelCategorias[]>();
  mensaje = new Subject<string>();

  constructor(public http: HttpClient) {

  }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  getCategorys(): Observable<ModelCategorias[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelCategorias[]>(`${this.apiUrl}/getAll`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  getCategorysPageable(page: number, size: number): Observable<ModelCategorias[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelCategorias[]>(`${this.apiUrl}/pageable?page=${page}&size=${size}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  // Otra Forma
  /*getCategorys2(): Observable<ModelCategorias[]> {
    return this.http.get(this.apiUrl + '/getAll').pipe(
      map((response) => response as ModelCategorias[])
    );

  }*/


  public saveCategory(modelCategory: ModelCategorias): Observable<ModelCategorias> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post<ModelCategorias>(`${this.apiUrl}/save`, modelCategory, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  public getCategoryId(id: any): Observable<ModelCategorias> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelCategorias>(`${this.apiUrl}/listar/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  public updateCategory(modelCategory: ModelCategorias) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put<ModelCategorias>(`${this.apiUrl}/update`, modelCategory, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public delete(key$: any): Observable<ModelCategorias> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete<ModelCategorias>(`${this.apiUrl}/delete/${key$}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  public count(): Observable<ModelCategorias[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelCategorias[]>(`${this.apiUrl}/c`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });

  }

  /*search(q: string): Observable<any> {
    return this.getAll().pipe(
      map((data: any) => data
        .filter(item => JSON.stringify(item).toLowerCase().includes(q)))
    );
  }*/

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
