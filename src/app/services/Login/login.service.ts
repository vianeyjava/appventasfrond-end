import { Injectable } from '@angular/core';
import { AppSettings } from '../../app.settings';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { of, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { TOKEN_NAME, TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, HOST } from '../../components/shared/var.constant';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // private apiUrl = AppSettings.API_ENDPOINT + 'oauth/token';
  private apiUrl: string = `${HOST}oauth/token`;
  private apiUrlBorraTokenDB: string = `${HOST}Usuario`;

  constructor(private http: HttpClient, private router: Router) { }

  loginUserToken(usuario: string, contrasena: string) {
    const body = `grant_type=password&username=${encodeURIComponent(usuario)}&password=${encodeURIComponent(contrasena)}`;
    return this.http.post(this.apiUrl, body, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8').set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
    });
  }

  estaLogeado() {
    const token = sessionStorage.getItem(TOKEN_NAME);
    return token != null;
  }

  /*cerrarSesion() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }*/
  cerrarSesion() {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    this.http.get(`${this.apiUrlBorraTokenDB}/anular/${access_token}`).subscribe(data => {
      sessionStorage.clear();
      this.router.navigate(['login']);
    });
  }
}
