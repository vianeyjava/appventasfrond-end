import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../app.settings';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ModelProducts } from '../../models/inventory/products/model-products';
import { catchError } from 'rxjs/operators';
import { TOKEN_NAME, HOST } from '../../components/shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  // private apiUrl = AppSettings.API_ENDPOINT + 'Producto';
  private apiUrl: string = `${HOST}Producto`;

  constructor(private http: HttpClient) { }

  private httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getProducts(): Observable<ModelProducts[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelProducts[]>(`${this.apiUrl}/getAll`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  // Otra Forma
  /*getProducts2(): Observable<ModelProducts[]> {
    return this.http.get(this.apiUrl + '/getAll').pipe(
      map((response) => response as ModelProducts[])
    );
  }*/
  getProductId(id: any): Observable<ModelProducts> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelProducts>(`${this.apiUrl}/listar/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  /*public getProductId(id: number) {
    const url = `${this.apiUrl}/listar/${id}`;
    return this.http.get<ModelProducts>(url).pipe(
      catchError(this.handleError));
  }*/
  /*public saveProduct2(modelProducts: ModelProducts): Observable<ModelProducts> {
    return this.http.post<ModelProducts>(`${this.apiUrl}/save`, JSON.stringify(modelProducts), { headers: this.httpHeaders });
  }*/

  public saveProduct(modelProduct: ModelProducts): Observable<ModelProducts> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post<ModelProducts>(`${this.apiUrl}/save`, modelProduct, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public updateProduct(modelP: ModelProducts): Observable<ModelProducts> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put<ModelProducts>(`${this.apiUrl}/update`, modelP, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public deleteProduct(key$: any): Observable<ModelProducts> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete<ModelProducts>(`${this.apiUrl}/delete/${key$}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public count(): Observable<ModelProducts[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelProducts[]>(`${this.apiUrl}/c`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
