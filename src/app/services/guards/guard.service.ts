import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
//import { Observable } from 'rxjs';
//import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
//import { catchError } from 'rxjs/operators';
import { tokenNotExpired } from 'angular2-jwt';
import * as decode from 'jwt-decode';
import { LoginService } from '../Login/login.service';
import { MenusService } from '../menus/menus.service';
import { TOKEN_NAME } from '../../components/shared/var.constant';
import { ModelMenu } from '../../models/menus/ModelMenu';

@Injectable({
  providedIn: 'root'
})
export class GuardService implements CanActivate {

  constructor(private loginService: LoginService, 
    private menuService: MenusService, 
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let rpta = this.loginService.estaLogeado();
    if (!rpta) {
      sessionStorage.clear();
      this.router.navigate(['login']);
      return false;
    } else {
      let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
      if (tokenNotExpired(TOKEN_NAME, token.access_token)) {
        const decodedToken = decode(token.access_token);
        //console.log(decodedToken);
        let roles = decodedToken.authorities;
        //console.log(roles);
        let url = state.url;

        return this.menuService.listarPorUsuario(decodedToken.user_name).pipe(map((data: ModelMenu[]) => {
          this.menuService.menuCambio.next(data);

          let cont = 0;
          for (let m of data) {
            if (m.url === url) {
              cont++;
              break;
            }
          }

          if (cont > 0) {
            return true;
          } else {
            this.router.navigate(['not-403']);
            return false;
          }
        }));
      } else {
        sessionStorage.clear();
        this.router.navigate(['login']);
        return false;
      }
    }
  }
}
