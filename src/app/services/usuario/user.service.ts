import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from '../../app.settings';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { ModelUsers } from '../../models/usuario/ModelUsers';
import { TOKEN_NAME, HOST } from '../../components/shared/var.constant';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  // private apiUrl = AppSettings.API_ENDPOINT + 'Usuario';
  private apiUrl: string = `${HOST}Usuario`;

  constructor(public http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json'
  });


  public getUsers(): Observable<ModelUsers[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelUsers[]>(`${this.apiUrl}/getAll`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  // Otra Forma
  /*getUsers2(): Observable<ModelUsers[]> {
    return this.http.get(this.apiUrl + '/getAll').pipe(
      map((response) => response as ModelUsers[])
    );
  }*/
  /*public saveUsers(modelUser: ModelUsers): Observable<ModelUsers> {
    const getHeaders: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post<ModelUsers>(`${this.apiUrl}/save/`, modelUser, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }*/
  public saveUser2(modelUser: ModelUsers): Observable<ModelUsers> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.post<ModelUsers>(`${this.apiUrl}/save`, modelUser, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public updateUser(modelUser: ModelUsers, key$: any): Observable<ModelUsers> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.put<ModelUsers>(`${this.apiUrl}/update`, modelUser, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public deleteUser($Key: any): Observable<ModelUsers> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.delete<ModelUsers>(`${this.apiUrl}/delete/${$Key}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public getUserId(id: any): Observable<ModelUsers> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelUsers>(`${this.apiUrl}/listar/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }
  public count(): Observable<ModelUsers[]> {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<ModelUsers[]>(`${this.apiUrl}/count`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }


}
