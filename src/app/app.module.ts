import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import { NopagefoundComponent } from './components/shared/nopagefound/nopagefound.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { GraficoBarraComponent } from './components/graficos/grafico-barra/grafico-barra.component';
import { GraficoDonaComponent } from './components/graficos/grafico-dona/grafico-dona.component';
import { SidenavComponent } from './components/shared/sidenav/sidenav.component';
import { BreadcrumbsComponent } from './components/shared/breadcrumbs/breadcrumbs.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { CategorysComponent } from './components/inventory/categorys/categorys.component';
import { ManagerCategorysComponent } from './components/inventory/categorys/manager-categorys/manager-categorys.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { MaestroUsuariosComponent } from './components/usuarios/maestro-usuarios/maestro-usuarios.component';
import { NoPageComponent } from './components/no-page/no-page.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { ProductsComponent } from './components/inventory/products/products.component';
import { ManagerProductsComponent } from './components/inventory/products/manager-products/manager-products.component';
import { Not403Component } from './components/not403/not403.component';

// Rutas
import { AppRoutingModule } from './app-routing.module';

// Servicios
// import { CategorysService } from './services/categorias/categorys.service';
import { ServerErrorsInterceptor } from './components/shared/ServerErrorsInterceptor';


// Modulo propio para Angular Material
import { PropioMaterialModule } from './propio-material.module';


@NgModule({
  declarations: [
    AppComponent,
    GraficoBarraComponent,
    LoginComponent,
    AboutComponent,
    NopagefoundComponent,
    NavbarComponent,
    HomeComponent,
    GraficoDonaComponent,
    SidenavComponent,
    BreadcrumbsComponent,
    HeaderComponent,
    CategorysComponent,
    ManagerCategorysComponent,
    UsuariosComponent,
    MaestroUsuariosComponent,
    InventoryComponent,
    ProductsComponent,
    ManagerProductsComponent,
    NoPageComponent,
    Not403Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PropioMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: ServerErrorsInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
