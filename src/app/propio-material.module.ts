import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; // para *ngIf, *ngFor y otras directivas

// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
/* Import the module*/
import { NgMaterialMultilevelMenuModule } from 'ng-material-multilevel-menu';
import { MatNativeDateModule, MatAutocompleteModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatCardModule,
    MatSelectModule,
    MatDividerModule,
    MatIconModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatMenuModule,
    MatExpansionModule,
    MatListModule,
    MatDatepickerModule,
    MatSortModule,
    MatSnackBarModule,
    MatDialogModule,
    NgMaterialMultilevelMenuModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule
  ],
  declarations: [],
  exports: [
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatCardModule,
    MatSelectModule,
    MatDividerModule,
    MatIconModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatMenuModule,
    MatExpansionModule,
    MatListModule,
    MatDatepickerModule,
    MatSortModule,
    MatSnackBarModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    NgMaterialMultilevelMenuModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ]
})
export class PropioMaterialModule { }
