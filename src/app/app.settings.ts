import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AppSettings {


    // public static API_ENDPOINT = 'http://192.168.1.147:8080/';
    public static API_ENDPOINT = 'http://192.168.0.11:8888/';
    //public static API_ENDPOINT = 'http://172.18.175.168:8888/';
    // public static API_ENDPOINT = 'http://181.143.98.170:8089/POSSQLWEB/';
    // public static API_ENDPOINT = 'http://172.18.175.109:8080/POSSQLWEB/';
    // public static API_ENDPOINT = 'http://192.168.0.35:8080/POSSQLWEB/';
    // public static API_ENDPOINT = 'http://172.18.175.235:8080/POSSQLWEB/';
    // public static API_ENDPOINT = 'http://181.143.98.170:8181/POSSQLWEB/';
    // public static API_ENDPOINT = 'http://localhost:8080/POSSQLWEB/';


    constructor(private http: HttpClient) {

    }
}
