import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ProductsService } from '../../../services/products/products.service';
import { IProducts } from '../../../interfaces/IProducts';
import { ModelProducts } from '../../../models/inventory/products/model-products';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  // public listProducts: IProducts[];
  public listProducts: ModelProducts[] = [];
  public dataSource: MatTableDataSource<ModelProducts>;
  public displayedColumns = ['referenceProduct', 'marca', 'description', 'habilitado', 'categorias', 'acciones'];

  constructor(private _serviceProduct: ProductsService) {
  }


  ngOnInit() {
    this.getProducts();
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  getProducts() {
    this._serviceProduct.getProducts()
      .subscribe(data => {
        this.listProducts = data;
        this.dataSource = new MatTableDataSource(this.listProducts);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }
  deleteProduct(modelProduct: ModelProducts): void {
    this._serviceProduct.deleteProduct(modelProduct.idProducto)
      .subscribe(resp => {
        this.listProducts = this.listProducts.filter(filterr => filterr !== modelProduct);
        console.log('Exito', 'Producto Eliminado con exito!', resp);
        // this.getCategorys();
      });
  }

}
