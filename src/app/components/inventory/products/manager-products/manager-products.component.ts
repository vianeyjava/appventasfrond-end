import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormControl } from '@angular/forms';
import { ModelProducts } from '../../../../models/inventory/products/model-products';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ProductsService } from '../../../../services/products/products.service';
import { CategorysService } from '../../../../services/categorias/categorys.service';
import { error } from '@angular/compiler/src/util';
import { ModelCategorias } from '../../../../models/inventory/categorias/ModelCategorys';

@Component({
  selector: 'app-manager-products',
  templateUrl: './manager-products.component.html',
  styleUrls: ['./manager-products.component.css']
})
export class ManagerProductsComponent implements OnInit {

  formProducts: FormGroup;
  public modelProduct: ModelProducts;
  cod: any;
  routerId: number;
  opcion1 = false;
  edicion: boolean = false;
  public listCategorys: ModelCategorias[] = [];
  idCategoriaSeleccionada: number;
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  constructor(
    private formBuilder: FormBuilder,
    private activateRouter: ActivatedRoute,
    private router: Router,
    private _serviceProduct: ProductsService,
    private _serviceCategory: CategorysService) {
    this.modelProduct = new ModelProducts();
    this.formProducts = new FormGroup({
      'idProducto': new FormControl(0),
      'referenceProduct': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'marca': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'description': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'nroManifiesto': new FormControl('', [Validators.required, Validators.minLength(5)]),
      'iva': new FormControl('', [Validators.required]),
      'descuentoProd': new FormControl('', [Validators.required]),
      'nota': new FormControl(''),
      'idCategoriaSeleccionada': new FormControl('', [Validators.required]),
      'state': new FormGroup({
        enable: new FormControl(true),
        active: new FormControl(true)
      }),
      'signature': new FormGroup({
        signatureCreate: new FormControl('VIANEY'),
        signaturelastModified: new FormControl('ALEX'),
        createdDate: new FormControl('2018-07-11T09:28:16.000Z'),
        lastModifiedDate: new FormControl('2018-07-11T09:29:16.000Z')
      })
    });
  }

  ngOnInit() {
    // obtenemos parametros por id
    this.activateRouter.params.subscribe((params: Params) => {
      this.routerId = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
    this.getCategorys();
  }


  private initForm() {
    if (this.edicion) {
      this._serviceProduct.getProductId(this.routerId).subscribe(data => {
        const id = data.idProducto;
        const referencia = data.referenceProduct;
        const marca = data.marca;
        const description = data.description;
        const manifiesto = data.nroManifiesto;
        const iva = data.iva;
        const descuentoP = data.descuentoProd;
        const categoriass = data.categorias;
        const stado = data.state.active;
        const signatur = data.signature;
        this.formProducts = this.formBuilder.group({
          'idProducto': new FormControl(id),
          'referenceProduct': new FormControl(referencia),
          'marca': new FormControl(marca),
          'nroManifiesto': new FormControl(description),
          'iva': new FormControl(iva),
          'descuentoProd': new FormControl(descuentoP),
          'idCategoriaSeleccionada': new FormControl(this.idCategoriaSeleccionada),
          'state': new FormGroup({
            enable: new FormControl(true),
            active: new FormControl(true)
          }),
          'signature': new FormGroup({
            signatureCreate: new FormControl('VIANEY'),
            signaturelastModified: new FormControl('ALEX'),
            createdDate: new FormControl('2018-07-11T09:28:16.000Z'),
            lastModifiedDate: new FormControl('2018-07-11T09:29:16.000Z')
          })
        });
      });
    }
  }

  // Obtenemos las categorias del servicio
  getCategorys() {
    this._serviceCategory.getCategorys()
      .subscribe(data => {
        this.listCategorys = data;
      });
  }
  // Guardamos productos
  @HostListener('window:keydown.control.y')
  saveData() {
    this.modelProduct.idProducto = this.formProducts.value['idProducto'];
    this.modelProduct.referenceProduct = this.formProducts.value['referenceProduct'];
    this.modelProduct.marca = this.formProducts.value['marca'];
    this.modelProduct.nroManifiesto = this.formProducts.value['nroManifiesto'];
    this.modelProduct.iva = this.formProducts.value['iva'];
    this.modelProduct.descuentoProd = this.formProducts.value['descuentoProd'];
    this.modelProduct.categorias = this.formProducts.value['categorias'];
    this.modelProduct.state = this.formProducts.value['state'];
    this.modelProduct.signature = this.formProducts.value['signature'];
    if (this.edicion) {
      // Actualizando
      this._serviceProduct.updateProduct(this.modelProduct).subscribe(data => {
        console.log(data);
        this._serviceCategory.getCategorys().subscribe(datos => {
          this._serviceCategory.categoryCambio.next(datos);
          this._serviceCategory.mensaje.next('Producto Actualizado');
        });
      }, error => {
        console.error('No se pudo Actualizar el Producto', error);
      });
    } else {
      // Insert
      this._serviceProduct.saveProduct(this.modelProduct).subscribe(data => {
        console.log(data);
        this._serviceCategory.getCategorys().subscribe(dataCategory => {
          this._serviceCategory.categoryCambio.next(dataCategory);
          this._serviceCategory.mensaje.next('Cambios Grabados Correctamente');
        });
        // this.formCategory.controls['nameCategory'].setValue('')
      }, error => {
        console.error('No se pudo Guardar el Producto', error);
      });
    }
    this.router.navigate(['/inventory/products']);
  }

}
