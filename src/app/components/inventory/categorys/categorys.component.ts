import { Component, OnInit, ViewChild } from '@angular/core';
import { CategorysService } from '../../../services/categorias/categorys.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { ModelCategorias } from '../../../models/inventory/categorias/ModelCategorys';
import { ICategorias } from '../../../interfaces/ICategorias';
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-categorys',
  templateUrl: './categorys.component.html',
  styleUrls: ['./categorys.component.css']
})
export class CategorysComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public dataSource: MatTableDataSource<ModelCategorias>;
  cantidad: number;
  // public listCategorys: any = [];
  public listCategorys: ModelCategorias[] = [];

  constructor(
    private _serviceCategory: CategorysService,
    private router: Router) {
  }

  // public displayedColumns = ['idCategory', 'nameCategory', 'Editar', 'Eliminar'];
  public displayedColumns = ['idCategory', 'nameCategory', 'acciones'];

  ngOnInit() {
    this.getCategorys();
  }

  getCategorys() {
    /*this._serviceCategory.getCategorys()
      .subscribe(data => {
        this.listCategorys = data;
        this.dataSource = new MatTableDataSource(this.listCategorys);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });*/
    this._serviceCategory.getCategorysPageable(0, 10).subscribe(data => {
      const listCat = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(listCat);
      this.dataSource.sort = this.sort;
    });
  }
  mostrarMas(event: any) {
    console.log(event);
    this._serviceCategory.getCategorysPageable(event.pageIndex, event.pageSize).subscribe(data => {
      const listCat = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(listCat);
      this.dataSource.sort = this.sort;
    });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /*deleteCategorys(index: number, key$: number) {
    this._serviceCategory.delete(key$)
      .subscribe(resp => {
        this.listCategorys.splice(index, 1);
        console.log('Exito', 'Categoría Eliminada con exito!', resp);
      });
  }*/
  deleteCategorys(idCategory: number): void {
    this._serviceCategory.delete(idCategory).subscribe(resp => {
      this._serviceCategory.getCategorys().subscribe(data => {
        this.listCategorys = data;
        this.dataSource = new MatTableDataSource(this.listCategorys);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    });
  }

  navigateTo(id: any) {
    // this.router.navigate(['/home/managerCategorys/edit/', row.idCategory]);
    this.router.navigate(['/inventory/managerCategorys/edit/', id]);
  }
}
