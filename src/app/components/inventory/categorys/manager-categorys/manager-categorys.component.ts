import { Component, OnInit, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CategorysService } from '../../../../services/categorias/categorys.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ModelCategorias } from '../../../../models/inventory/categorias/ModelCategorys';
import { MatSnackBar } from '@angular/material';
import { DataState } from '../../../../models/information/DataState';
import { DataSignature } from '../../../../models/information/DataSignature';
// declare var jQuery: any;
// declare var $: any;

@Component({
  selector: 'app-manager-categorys',
  templateUrl: './manager-categorys.component.html',
  styleUrls: ['./manager-categorys.component.css']
})
export class ManagerCategorysComponent implements OnInit {

  formCategorys: FormGroup;
  public modelCategory: ModelCategorias;
  public idCategoriaSeleccionada: number;
  cod: any;
  routerId: number;
  date: any;
  edicion: boolean = false;

  constructor(
    private _serviceCategory: CategorysService,
    public activateRouter: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private _snacBarMsg: MatSnackBar) {
    this.modelCategory = new ModelCategorias();
    this.formCategorys = this.formBuilder.group({
      'idCategory': [0, []],
      'nameCategory': ['', [Validators.required, Validators.minLength(5)]],
      'state': this.formBuilder.group({
        enable: true,
        active: true
      }),
      'signature': {
        signatureCreate: 'VIANEY',
        signaturelastModified: 'ALEX',
        createdDate: '2018-07-11T09:28:16.000Z',
        lastModifiedDate: '2018-07-11T09:29:16.000Z'
      }
    });
    /*this.formCategorys = new FormGroup({
      'idCategory': new FormControl(0),
      'nameCategory': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'state': new FormGroup({
        enable: new FormControl(true),
        active: new FormControl(true)
      }),
      'signature': new FormGroup({
        signatureCreate: new FormControl('VIANEY'),
        signaturelastModified: new FormControl('ALEX'),
        createdDate: new FormControl('2018-07-11T09:28:16.000Z'),
        lastModifiedDate: new FormControl('2018-07-11T09:29:16.000Z')
      })
    });*/
  }

  ngOnInit() {
    // obtenemos parametros por id
    this.activateRouter.params.subscribe((params: Params) => {
      this.routerId = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }
  private initForm() {
    if (this.edicion) {
      this._serviceCategory.getCategoryId(this.routerId).subscribe(data => {
        const idCat = data.idCategory;
        const nombre = data.nameCategory;
        const estado = data.state;
        const signature = data.signature;
        this.formCategorys = new FormGroup({
          'idCategory': new FormControl(idCat),
          'nameCategory': new FormControl(nombre),
          'state': new FormControl(estado),
          'signature': new FormControl(signature)
        });
      });
    }
  }


  /*getCategoryId2() {
    // obtenemos parametros por id
    this.routerId = this.activateRouter.snapshot.params['id'];
    if (this.routerId != null) {
      this._serviceCategory.getCategoryId2(this.routerId)
        .subscribe(parameters => {
          this.modelCategory = parameters;
          console.log('PARAMETROS ', parameters);
        });
    }
  }*/


  @HostListener('window:keydown.control.y')
  saveData() {
    this.modelCategory.idCategory = this.formCategorys.value['idCategory'];
    this.modelCategory.nameCategory = this.formCategorys.value['nameCategory'];
    this.modelCategory.state = this.formCategorys.value['state'];
    this.modelCategory.state = this.formCategorys.value['signature'];
    if (this.edicion) {
      // Actualizando
      this._serviceCategory.updateCategory(this.modelCategory).subscribe(data => {
        console.log(data);
        this._serviceCategory.getCategorys().subscribe(datos => {
          this._serviceCategory.categoryCambio.next(datos);
          this._serviceCategory.mensaje.next('Categoria Actualizada');
        });
      }, error => {
        console.error('No se pudo Actualizar la Categoria', error);
      });
    } else {
      // Insert
      this._serviceCategory.saveCategory(this.modelCategory).subscribe(data => {
        console.log(data);
        this._serviceCategory.getCategorys().subscribe(dataCategory => {
          this._serviceCategory.categoryCambio.next(dataCategory);
          this._serviceCategory.mensaje.next('Cambios Grabados Correctamente');
        });
        // this.formCategory.controls['nameCategory'].setValue('')
      }, error => {
        console.error('No se pudo Guardar la Categoria', error);
      });
    }
    this.router.navigate(['/inventory/categorys']);
  }

  count() {
    this._serviceCategory.count()
      .subscribe(data => {
        this.cod = data;
        this.formCategorys.controls['idCategory'].setValue(this.cod + 1);
      });
  }


}
