import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/Login/login.service';
import { Router } from '@angular/router';
import { TOKEN_NAME } from '../shared/var.constant';
import { MenusService } from '../../services/menus/menus.service';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: any;
  clave: any;

  constructor(
    private _serviceLogin: LoginService,
    private _menuService: MenusService,
    private router: Router) { }

  ngOnInit() {
  }
  /*ngAfterViewInit() {
    (window as any).initialize();
  }*/

  iniciarSesion(): void {
    this._serviceLogin.loginUserToken(this.usuario, this.clave).subscribe(data => {
      //console.log(data);
      if (data) {
        let token = JSON.stringify(data);
        sessionStorage.setItem(TOKEN_NAME, token);
        this.router.navigate(['/home']);

        let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        /*decode es una libreria que me sirve para decodificar y analizar el token y la estructura que lleva dentro
        para instalar la libreria es: npm install jwt-decode Lo que hace es que una vez tengamos el token esa cadena
        larga lo va a decodificar y en esta linea siguiente obtendremos el token decodificado.*/
        const decodedToken = decode(tk.access_token);
        console.log(decodedToken);//Mostramos el token decodificado

        /*Aquí en esta linea ya viene el 
        nombre de usuario que viene dentro del token pero ya decodificado.*/
        this._menuService.listarPorUsuario(decodedToken.user_name).subscribe(data => {
          this._menuService.menuCambio.next(data);
        });
        console.log(decodedToken.authorities);//Mostramos los Roles que tiene el usuario

        /*Estas línea de abajo con el for es Opcional, por si queremos colocarlo pero no es necesidad. */
        /*let roles = decodedToken.authorities;
        for (let i = 0; roles.length; i++) {
          let rol = roles[i];
          if (rol === 'ADMIN') {
            this.router.navigate(['/inventory/categorys']);
            break;
          } else {
            this.router.navigate(['/inventory/managerCategorys/new']);
            break;
          }
        }*/
      }
    });
  }

}
