import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grafico-dona',
  templateUrl: './grafico-dona.component.html',
  styleUrls: ['./grafico-dona.component.css']
})
export class GraficoDonaComponent implements OnInit {

  view: number[] = [700, 300];
  data: any[] = [];
  isRealtime: boolean;
  showXAxisLabel: boolean;
  showYAxisLabel: boolean;
  showLegend: boolean;
  interval: number;

  public saludo: string;

  constructor() { }

  ngOnInit() {
    this.data = this.generateData();
  }

  private generateData() {
    return [
      {
        'name': 'Germany',
        'value': 31229
      },
      {
        'name': 'United States',
        'value': 19869
      },
      {
        'name': 'France',
        'value': 21359
      },
      {
        'name': 'United Kingdom',
        'value': 20598
      },
      {
        'name': 'Spain',
        'value': 56009
      },
      {
        'name': 'Italy',
        'value': 24090
      }
    ];
  }
  teclear(event) {
    // console.log(event.tarjet.value);
    this.saludo = event.tarjet.value;
  }
}
