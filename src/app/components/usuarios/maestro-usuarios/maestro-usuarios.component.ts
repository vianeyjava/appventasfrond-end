import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModelUsers } from '../../../models/usuario/ModelUsers';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../services/usuario/user.service';

@Component({
  selector: 'app-maestro-usuarios',
  templateUrl: './maestro-usuarios.component.html',
  styleUrls: ['./maestro-usuarios.component.css']
})
export class MaestroUsuariosComponent implements OnInit {

  formUser: FormGroup;
  public modelUser: ModelUsers;
  hide = true;
  disabled = false;
  cod: any;
  routerId: any;

  constructor(
    private _serviceUser: UserService,
    public activateRouter: ActivatedRoute,
    private router: Router,
    public formBuilder: FormBuilder) {
    this.modelUser = new ModelUsers();

    this.formUser = this.formBuilder.group({
      'idUsuario': ['', [Validators.required, Validators.minLength(1)]],
      'nombres': ['', [Validators.required, Validators.minLength(5)]],
      'apellidos': ['', [Validators.required, Validators.minLength(5)]],
      'usuario': ['', [Validators.required, Validators.minLength(5)]],
      'clave': ['', [Validators.required, Validators.minLength(3)]],
      'email': ['', [Validators.required, Validators.email]],
      'estado': ['true']
    });
  }
  ngOnInit() {
    console.log('Metodo ngOnInit');
    this.getUserId();
    this.count();
  }
  /*getUserId2() {
    // obtenemos parametros por id
    this.routerId = this.activateRouter.snapshot.params['id'];
    if (this.routerId != null) {
      this._serviceUser.getUserId(this.routerId)
        .subscribe(parameters => {
          this.modelUser = parameters;
          console.log('PARAMETROS ', parameters);
        });
    }
  }*/
  getUserId() {
    // obtenemos parametros por id
    this.activateRouter.params.subscribe(parametros => {
      this.routerId = parametros['id'];
      if (this.routerId) {
        this._serviceUser.getUserId(this.routerId).subscribe(parameter => {
          this.modelUser = parameter;
          console.log('PARAMETROS ', parameter);
        });
      }
    });
  }
  saveData(form: any) {
    if (this.formUser.valid) {
      // insertando
      this._serviceUser.saveUser2(this.modelUser)
        .subscribe(data => {
          // this.formCategory.controls['nameCategory'].setValue('')
          this.count();
          /*this.notification.success(
            'Exito!',
            'Cambios Grabados Correctamente'
          );*/
          console.log('Cambios Grabados Correctamente');
          this.router.navigate(['/usuarios']);
        }, error => {
          /*this.notification.error(
              'Error',
              'No se pudo Guardar los Cambios'
          );*/
          console.error('No se pudo Guardar el Usuario', error);
        });
    } else {
      // Actualizando
      this._serviceUser.updateUser(this.modelUser, this.routerId)
        .subscribe(data => {
          console.log('Usuario Actualizado');
        }, error => {
          console.error('No se pudo Actualizar el Usuario', error);
        });
    }
  }

  count() {
    this._serviceUser.count()
      .subscribe(data => {
        this.cod = data;
        // this.formUser.controls['idUsuario'].setValue(this.cod + 1);
        this.formUser.controls['idUsuario'].setValue(this.cod + 1);

      });
  }

}
