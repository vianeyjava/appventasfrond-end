import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }





  navigateTo(id: any) {
    // this.router.navigate(['/home/managerUser/edit/', row.id]);
    this.router.navigate(['/managerUser/edit/', id]);
  }

}
