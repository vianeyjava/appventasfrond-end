export interface IProducts {
    referenceProduct: string;
    marca: string;
    description: string;
    habilitado: boolean;
}
