import { DataState } from '../models/information/DataState';
import { DataSignature } from '../models/information/DataSignature';

export interface ICategorias {
    idCategory: number;
    nameCategory: string;
    state: DataState;
    signature: DataSignature;

}
