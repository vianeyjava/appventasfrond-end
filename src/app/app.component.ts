import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenusService } from './services/menus/menus.service';
import { ModelMenu } from './models/menus/ModelMenu';
import { LoginService } from './services/Login/login.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // opened = false;
  listMenus: ModelMenu[] = [];
  constructor(private router: Router,
    private _serviceMenu: MenusService,
    public loginService: LoginService) {

  }

  config = {
    paddingAtStart: true,
    classname: 'my-custom-class',
    listBackgroundColor: '(128, 170, 255)',
    fontColor: 'rgb(0, 34, 102)',
    backgroundColor: '(128, 170, 255)',
    selectedListFontColor: 'red',
  };

  /*appitems = [
    {
      label: 'Perfíl',
      faIcon: 'fab fa-500px',
      items: [
        {
          label: 'Mi Perfíl',
          link: '/inventory/categorys',
          faIcon: 'fab fa-accusoft'
        },
        {
          label: 'Usuarios',
          faIcon: 'fab fa-accessible-icon',
          items: [
            {
              label: 'Registro de Usuarios',
              link: '/item-1-2-1',
              faIcon: 'fas fa-allergies'
            },
            {
              label: 'Permisos',
              faIcon: 'fas fa-ambulance',
              items: [
                {
                  label: 'Item 1.2.2.1',
                  link: 'item-1-2-2-1',
                  faIcon: 'fas fa-anchor'
                }
              ]
            }
          ]
        }
      ]
    },
    {
      label: 'Maestros',
      icon: 'alarm',
      items: [
        {
          label: 'Inventario',
          faIcon: 'fab fa-accessible-icon',
          items: [
            {
              label: 'Tipo Inventario',
              link: '/item-1-2-1',
              faIcon: 'fas fa-allergies'
            },
            {
              label: 'Tipo Movimiento',
              link: '/item-1-2-1',
              faIcon: 'fab fa-accessible-icon'
            },
            {
              label: 'Categorías',
              link: '/item-1-2-1',
              faIcon: 'fas fa-allergies'
            },
          ]
        }
      ]
    },
    {
      label: 'Documentos',
      faIcon: 'fab fa-500px',
      items: [
        {
          label: 'Compras',
          link: '/inventory/categorys',
          faIcon: 'fab fa-accusoft'
        },
        {
          label: 'Facturas',
          link: '/inventory/categorys',
          faIcon: 'fab fa-accusoft'
        },
        {
          label: 'Notas Crédito',
          link: '/inventory/categorys',
          faIcon: 'fab fa-accusoft'
        }
      ]
    },
    {
      label: 'Item 3',
      link: '/item-3',
      icon: 'offline_pin'
    },
    {
      label: 'Item 4',
      link: '/item-4',
      icon: 'star_rate',
      hidden: false
    }
  ];*/

  ngOnInit() {
    this.getMenus();
  }
  selectedItem($event) {
    console.log($event);
    this.router.navigate([$event]);
  }

  /*En esta linea this._serviceMenu.getMenus() estamos llamando a todos los menus
  Pero NO queremos llamar a todos los menus, solo quiero llamar a menuCambio, es decir al Objeto reactivo
  porque cuando su Data sea llenado, ésto va a reaccionar y va a poblar los Menús. */
  getMenus() {
    /*this._serviceMenu.getMenus().subscribe(data => {
      this.listMenus = data;
      console.log(this.listMenus);
    });*/
    this._serviceMenu.menuCambio.subscribe(data => {
      this.listMenus = data;
    });
  }
}
