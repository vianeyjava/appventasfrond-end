import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import { HomeComponent } from './components/home/home.component';
import { GraficoBarraComponent } from './components/graficos/grafico-barra/grafico-barra.component';
import { GraficoDonaComponent } from './components/graficos/grafico-dona/grafico-dona.component';
import { CategorysComponent } from './components/inventory/categorys/categorys.component';
import { ManagerCategorysComponent } from './components/inventory/categorys/manager-categorys/manager-categorys.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { MaestroUsuariosComponent } from './components/usuarios/maestro-usuarios/maestro-usuarios.component';
import { NoPageComponent } from './components/no-page/no-page.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { ProductsComponent } from './components/inventory/products/products.component';
import { ManagerProductsComponent } from './components/inventory/products/manager-products/manager-products.component';
import { Not403Component } from './components/not403/not403.component';
import { GuardService } from './services/guards/guard.service';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'about', component: AboutComponent, canActivate: [GuardService] },
  { path: 'graficoBarra', component: GraficoBarraComponent, canActivate: [GuardService] },
  { path: 'graficoDona', component: GraficoDonaComponent , canActivate: [GuardService]},
  { path: 'not-403', component: Not403Component },

  // { path: 'categorys', component: CategorysComponent },
  // { path: 'managerCategorys/new', component: ManagerCategorysComponent },
  // { path: 'managerCategorys/edit/:id', component: ManagerCategorysComponent },
  {
    path: 'inventory', component: InventoryComponent, children: [
      { path: 'categorys', component: CategorysComponent },
      { path: 'managerCategorys/new', component: ManagerCategorysComponent },
      { path: 'managerCategorys/edit/:id', component: ManagerCategorysComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'managerProducts/new', component: ManagerProductsComponent },
      { path: 'managerProducts/edit/:id', component: ManagerProductsComponent },
    ], canActivate: [GuardService]
  },
  { path: 'usuarios', component: UsuariosComponent , canActivate: [GuardService]},
  { path: 'managerUser/new', component: MaestroUsuariosComponent , canActivate: [GuardService]},
  { path: 'managerUser/edit/:id', component: MaestroUsuariosComponent , canActivate: [GuardService]},

  // { path: '', pathMatch: 'full', redirectTo: 'home' },
  // { path: '**', component: NoPageComponent }
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: NoPageComponent }

];

@NgModule({
  imports: [
    RouterModule.forRoot(APP_ROUTES, { useHash: true })
  ],
  declarations: [

  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
