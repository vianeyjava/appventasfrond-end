import { ModelSubMenu } from './ModelSubMenu';
import { ModelRoles } from './ModelRoles';

export class ModelMenu {
    idMenu: number;
    icono: string;
    nombre: string;
    url: string;
    roles: ModelRoles[];

    constructor() {

    }

}