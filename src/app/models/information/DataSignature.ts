export class DataSignature {
    signatureCreate: string;
    signaturelastModified: string;
    createdDate: string;
    lastModifiedDate: string;
}
