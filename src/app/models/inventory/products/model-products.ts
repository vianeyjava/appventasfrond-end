import { ModelCategorias } from '../categorias/ModelCategorys';
import { DataState } from '../../information/DataState';
import { DataSignature } from '../../information/DataSignature';


export class ModelProducts {
    public idProducto: number;
    public referenceProduct: string;
    public marca: string;
    public description: string;
    public nroManifiesto: string;
    public iva: number;
    public descuentoProd: number;
    public nota: string;
    public categorias: ModelCategorias[];
    public state: DataState;
    public signature: DataSignature;

    constructor() {
        this.state = new DataState();
        this.signature = new DataSignature();
        this.categorias = new Array();
    }

}
