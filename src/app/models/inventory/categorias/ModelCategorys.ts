import { DataState } from '../../information/DataState';
import { DataSignature } from '../../information/DataSignature';

export class ModelCategorias {
    public idCategory: number;
    public nameCategory: string;
    public state: DataState;
    public signature: DataSignature;

    constructor() {
        this.state = new DataState();
        this.signature = new DataSignature();
    }
}
